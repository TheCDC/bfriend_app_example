#!/usr/bin/env python3
import flask
import time
import json

app = flask.Flask(__name__)


@app.route("/")
def hello():
    return """<p>Welcome to the BFriend app!</p>
<p>The current time is <b>{}</b></p>
<p><a href="/api">/api</a></p>
<p><a href="/echo/example/data/url">/echo/example/data/url</a></p>
<p><a href="/file/example_filename">/file/example_filename</a></p>
<p><a href="/file/invalid/path/for_this_endpoint">/file/invalid/path/for_this_endpoint</a></p>""".format(time.strftime("%Y-%m-%d %H-%M-%S"))


@app.route("/api")
def api():
    response = {"url": "/api"}
    return json.dumps(response)


@app.route("/echo/<path:data>")
def echo(data):
    return "You visited: " + repr(data)


@app.route("/file/<filename>")
def serve_file(filename):
    return "You requested this file: {}".format(repr(filename))


def main():
    app.run()

if __name__ == '__main__':
    main()
